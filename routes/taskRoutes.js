const express = require('express');
const router = express.Router();


const taskController = require('./../controllers/taskControllers')

// Method 2 
const {createTask,
    getAllTasks,
    deleteTask,
    updateTask,
    updateTaskByOne,
    getOneTask,
    updateTaskById} = require(`./../controllers/taskControllers`)


//CREATE A TASK

router.post(`/`, async (req, res) => {
    //console.log(req.body) //object

    //createTask(req.body).then(result => res.send(result
      
    
//Method 2
 await createTask(req.body).then(result => res.send(result))

}) ; 

//GET ALL TASK
router.get('/', async (req, res) =>{
    try{
        await getAllTasks().then(result => res.send(result))
    }
    catch(err){
        res.status(500).json(err)
    }
})

//DELETE
router.delete('/:id/delete', async (req,res) => {
    //console.log(req)
    try{
        await deleteTask(req.params.id).then(response => res.send(response))

    }catch(err){
        res.status(500).json(err)
    }

})

//UPDATE BY ID
router.put('/:taskId', async (req, res) => {
    const id= req.params.taskId
    await updateTask(id, req.body).then(response => res.send(response))
})


// FIND ONE AND UPDATE
router.put('/', async (req, res) => {
    await updateTaskByOne(req.body.name, req.body).then(response => res.send(response))
})


//FIND BY ID
router.get('/:taskId', async (req, res) => {
    const id= req.params.taskId
    await getOneTask(id).then(response => res.send(response))
})


//UPDATE BY ID
router.put('/:taskId/completed', async (req, res) => {
    const id= req.params.taskId
    await updateTaskById(id).then(response => res.send(response))
})

module.exports = router;