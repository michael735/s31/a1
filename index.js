const express = require('express');
const dotenv = require('dotenv');
const mongoose = require('mongoose');
dotenv.config();

const app = express();
const PORT = 4007;

// connect routes to index.js

const taskRoutes = require(`./routes/taskRoutes`)

//Middlewares
app.use(express.json())
app.use(express.urlencoded({extended:true}))

//Mongoose connection
mongoose.connect(process.env.MONGO_URL,{useNewUrlParser: true, useUnifiedTopology: true});


//DB connection notification
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log(`Connected to database`));


app.use(`/api/tasks`, taskRoutes)

app.listen(PORT, () => console.log(`Server connected to port ${PORT}`))
