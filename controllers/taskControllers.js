
const Task = require(`./../models/Task`)

// POST TASK
module.exports.createTask = async(reqBody) =>{
    return await Task.findOne({name: reqBody.name }).then((result, err) => {
        console.log(result)
        if(result != null && result.name == reqBody.name){
            return true
        }else {
            console.log(result)
            if(result == null){
                let newTask = new Task ({
                    name: reqBody.name
                })
                return newTask.save()
            }else{
                return err
            }
        }
    })
}

//GET ALL
module.exports.getAllTasks = async(reqBody) =>{


    return await Task.find().then( (result, err) => {
		if(result){
			return result
		} else {
			return err
		}
	})
}

//DELETE BY ID
module.exports.deleteTask = async(id) =>{
    return await Task.findByIdAndDelete(id).then(result => {
        try{
            if(result != null){
                return true
            }else{
                return false
            }
        }catch(err){
            return err
        }
    })
}

//UPDATE BY ID
module.exports.updateTask = async(id, reqBody) =>{
    console.log(reqBody)
    return await Task.findByIdAndUpdate(id, {$set: reqBody}, {new:true}).then(result => result)
}



//FIND BY ID
module.exports.getOneTask = async(id) =>{
    return await Task.findById(id).then((result, err) => {
		if(result != null){
			return result
		} else if(result == null){
			return `No existing document`
		} else{
            return err
        }
	})
}


// FIND ONE AND UPDATE
module.exports.updateTaskByOne = async(name, reqBody) => {
    return await Task.findOneAndUpdate({name: name}, {$set: reqBody}, {new:true})
	.then(response => {
		if(response == null){
			return {message: `no existing document`}
		} else {
			if(response != null){
				return response
			}
		}
	})
}

//FIND ID AND UPDATE
module.exports.updateTaskById = async(id) =>{
    return await Task.findByIdAndUpdate(id, {$set:{"status": "completed"}}, {new:true}).then(result => result)
}

